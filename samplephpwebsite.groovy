job('BranchV1') {
    scm {
        git{
            remote {
                    name('gitEvalJenkins')
                    url('https://gitlab.com/FranckRib/evalJenkins.git')
                }
            branch('V1')
        }
    }
    triggers {
        gitlabPush {
            buildOnPushEvents(true)
        }
    }
    steps {
        shell 'phpunit test.php'
    }
    steps {
        dockerBuildAndPublish {
            repositoryName('kranfc/V1')
            registryCredentials('docker-hub')
            forcePull(false)
            createFingerprints(false)
            skipDecorate()
        }
    }
}
job('BranchV2') {
    scm {
        git{
            remote {
                    name('gitEvalJenkins')
                    url('https://gitlab.com/FranckRib/evalJenkins.git')
                }
            branch('V2')
        }
    }
    triggers {
        gitlabPush {
            buildOnPushEvents(true)
        }
    }
    steps {
        shell 'phpunit test.php'
    }
    steps {
        dockerBuildAndPublish {
            repositoryName('kranfc/V2')
            registryCredentials('docker-hub')
            forcePull(false)
            createFingerprints(false)
            skipDecorate()
        }
    }
}
job('BranchV3') {
    scm {
        git{
            remote {
                    name('gitEvalJenkins')
                    url('https://gitlab.com/FranckRib/evalJenkins.git')
                }
            branch('V3')
        }
    }
    triggers {
        gitlabPush {
            buildOnPushEvents(true)
        }
    }
    steps {
        shell 'phpunit test.php'
    }
    steps {
        dockerBuildAndPublish {
            repositoryName('kranfc/V3')
            registryCredentials('docker-hub')
            forcePull(false)
            createFingerprints(false)
            skipDecorate()
        }
    }
}